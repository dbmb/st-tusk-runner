"use strict";

const gulp         = require('gulp'),
      del          = require('del'),
      rename       = require('gulp-rename'),
      babel        = require('gulp-babel'),
      concat       = require('gulp-concat'),
      uglify       = require('gulp-uglify'),
      jscs         = require('gulp-jscs'),
      sass         = require('gulp-sass'),
      autoPrefixer = require('gulp-autoprefixer'),
      bourbon      = require('node-bourbon'),
      cache        = require('gulp-cache');

const SOURCE_DIR = 'src/';
const DIST_DIR = 'dist/';
const source_path = {
        sass: `${SOURCE_DIR}sass/**/*.+(scss|sass)`,
        js:   `${SOURCE_DIR}app/**/*.js`,
        tpl:  `${SOURCE_DIR}index.html`
    };

gulp.task('sass', () => {
    return gulp.src(source_path.sass)
        .pipe(sass({
            includePaths: bourbon.includePaths
        }).on('error', sass.logError))
        .pipe(autoPrefixer(['last 15 versions']))
        .pipe(rename('style.css'))
        .pipe(gulp.dest(SOURCE_DIR))
});

gulp.task('babel', () => {
    return gulp.src(source_path.js)
        .pipe(babel({
            presets: [['env', {
                target: {"android": "4.2"},
                modules: false,
                debug: true,
                exclude: [
                    'transform-es2015-function-name',
                    'transform-es2015-computed-properties',
                    'transform-es2015-literals'
                ]
            }]]
        }))
        .pipe(concat('index.js'))
        .pipe(jscs({fix: true}))
        .pipe(gulp.dest(SOURCE_DIR))
});

gulp.task('lint', () => {
    return gulp.src(source_path.js)
        .pipe(jscs())
        .pipe(jscs.reporter())
});

gulp.task('cleanDist', () => {
    return del.sync('dist')
});

gulp.task('build', ['cleanDist', 'sass', 'babel'], () => {
    gulp.src(`${SOURCE_DIR}css/**/*.css`).pipe(gulp.dest(DIST_DIR));
    gulp.src(`${SOURCE_DIR}index.js`).pipe(gulp.dest(DIST_DIR));
    gulp.src(`${SOURCE_DIR}index.html`).pipe(gulp.dest(DIST_DIR));
});

gulp.task('watch', ['sass', 'babel', 'lint'], () => {
    gulp.watch(source_path.sass, ['sass']);
    gulp.watch(source_path.js, ['babel', 'lint']);
});

gulp.task('cache', () => {
   cache.clearAll()
});

gulp.task('default', ['watch']);
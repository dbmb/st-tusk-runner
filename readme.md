### Запустить watcher

`gulp`

### Собрать проект

`gulp build`

### Очистить кеш

`gulp cache`

### Использование 'Use strict'

В файле:
`/gulpfile.js`
Заменить в методе `useStrict` переменную `isEnabled`

#### Доки

1. [bourbon](https://www.bourbon.io/docs/5.0.0/)
